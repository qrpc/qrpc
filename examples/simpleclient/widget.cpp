/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#include "widget.h"
#include "ui_widget.h"

#include <QLocalSocket>

#include <qrpcclient.h>
#include <qrpcobject.h>
#include <qrpciodevicetransport.h>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget), socket(new QLocalSocket(this)), rpcClient(0)
{
    ui->setupUi(this);
    connectionLost();  // just to get the UI into the correct state

    // Connect to the server on button click
    connect(ui->connect, &QPushButton::clicked, [this]() {
        socket->connectToServer("qrpcsimpleserver");
    });

    // Handle connection events
    connect(socket, &QLocalSocket::connected, this, &Widget::connectionEstablished);
    connect(socket, &QLocalSocket::disconnected, this, &Widget::connectionLost);
}

Widget::~Widget()
{
    delete ui;
}

void Widget::connectionEstablished()
{
    // Create a transport...
    QRPCIODeviceTransport *transport = new QRPCIODeviceTransport(socket, this);
    // ... and a client communicating over the transport.
    QRPCClient *client = new QRPCClient(transport, this);

    // When we receive a remote object (in this example, it can only be the spinbox),
    // synchronise the state and connect the slider to it.
    connect(client, &QRPCClient::remoteObjectReady, [this](QRPCObject *object) {
        ui->horizontalSlider->setValue(object->property("value").toInt());
        ui->horizontalSlider->setMinimum(object->property("minimum").toInt());
        ui->horizontalSlider->setMaximum(object->property("maximum").toInt());

        connect(ui->horizontalSlider, SIGNAL(valueChanged(int)), object, SLOT(setValue(int)));
    });

    // Request the spinbox.
    client->requestRemoteObject("spinbox");

    // Clean up when we lose the connection.
    connect(socket, &QLocalSocket::disconnected, client, &QObject::deleteLater);
    connect(socket, &QLocalSocket::disconnected, transport, &QObject::deleteLater);

    // Hide unneeded UI elements
    ui->infoLabel->setVisible(true);
    ui->horizontalSlider->setVisible(true);
    ui->connectedLabel->setVisible(false);
    ui->connect->setVisible(false);
}

void Widget::connectionLost()
{
    // Unhide the "Connect" button and status label, hide the slider.
    ui->infoLabel->setVisible(false);
    ui->horizontalSlider->setVisible(false);
    ui->connectedLabel->setVisible(true);
    ui->connect->setVisible(true);
}
