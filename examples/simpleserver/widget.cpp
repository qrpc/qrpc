/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#include "widget.h"
#include "ui_widget.h"

#include <QLocalServer>
#include <QLocalSocket>

#include <qrpciodevicetransport.h>
#include <qrpcobjectexporter.h>
#include <qrpcserver.h>

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget), localServer(new QLocalServer(this)),
    exporter(new QRPCObjectExporter(this))
{
    ui->setupUi(this);

    // Export the QSpinBox with the identifier "spinbox".
    exporter->exportObject("spinbox", ui->spinBox);

    // Handle new connections, serve under the name "qrpcsimpleserver".
    connect(localServer, &QLocalServer::newConnection, this, &Widget::handleNewConnection);
    localServer->listen("qrpcsimpleserver");
}

Widget::~Widget()
{
    delete ui;
}

void Widget::handleNewConnection()
{
    // Get the connection socket
    QLocalSocket *socket = localServer->nextPendingConnection();

    // Create a transport...
    QRPCIODeviceTransport *transport = new QRPCIODeviceTransport(socket, socket);

    // ... and the server communicating over the transport, serving objects exported from the
    // exporter. Both the transport and the server are children of the socket so they get
    // properly cleaned up.
    new QRPCServer(exporter, transport, socket);
}
