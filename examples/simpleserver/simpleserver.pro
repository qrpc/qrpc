QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = simpleserver
TEMPLATE = app

INCLUDEPATH += $${QRPC_SOURCE_TREE}/src

win32 {
    CONFIG(debug, debug|release):LIBS += -L$${QRPC_BUILD_TREE}/src/debug -lqrpc
    CONFIG(release, debug|release):LIBS += -L$${QRPC_BUILD_TREE}/src/release -lqrpc
}
unix: LIBS += -L$${QRPC_BUILD_TREE}/src -lqrpc

SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui
