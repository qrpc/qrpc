TEMPLATE = subdirs

CONFIG += ordered

SUBDIRS += \
    src \
    examples

OTHER_FILES += \
    .qmake.conf
