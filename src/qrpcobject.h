/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCOBJECT_H
#define QRPCOBJECT_H

#include <QObject>
#include <QVariant>

#include "qrpc_global.h"

class QRPCClient;
class QRPCObjectPrivate;

/**
 * @brief The QRPCObject class represents a remote object.
 *
 * It's QMetaObject will mirror that of the remote object. Consequently, all meta-object based functionality like
 * signals, slots, invokables, properties and more are all available throught the QRPCObject.
 */
class QRPC_EXPORT QRPCObject : public QObject
{
public:
    /**
     * @brief Returns the remote object id of this remote object.
     */
    QString remoteObjectId() const;

    /**
     * @brief Returns the QRPCClient associated with this QRPCObject.
     */
    QRPCClient *rpcClient();

    const QMetaObject *metaObject() const;
    bool event(QEvent *event);

protected:
    QRPCObject(const QString &objectId, const QMetaObject *fakeMetaObject,
               const QMap<int, QVariant> &properties, QRPCClient *parent = 0);
    ~QRPCObject();

    QRPCObject(QRPCObjectPrivate &dd, QRPCClient *parent);

    int qt_metacall(QMetaObject::Call call, int id, void **o);

private:
    friend class QRPCClientPrivate;

    QRPCObjectPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(QRPCObject)
};

#endif // QRPCOBJECT_H
