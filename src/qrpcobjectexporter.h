/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCOBJECTEXPORTER_H
#define QRPCOBJECTEXPORTER_H

#include <QObject>
#include <QMap>

#include "qrpc_global.h"

class QRPCObjectExporterPrivate;

/**
 * @brief The QRPCObjectExporter class is used to centrally register exported objects.
 *
 * It relays signal emissions and property changes to QRPCServers.
 */
class QRPC_EXPORT QRPCObjectExporter : public QObject
{
    Q_OBJECT
    Q_PROPERTY(ObjectMap exportedObjects READ exportedObjects NOTIFY exportedObjectsChanged)
public:
    typedef QMap<QString, QObject*> ObjectMap;

    /**
     * @brief QRPCObjectExporter Constructs a new QRPCObjectExporter.
     * @param parent The parent object of the QRPCObjectExporter.
     */
    explicit QRPCObjectExporter(QObject *parent = 0);
    ~QRPCObjectExporter();

    /**
     * @brief exportObject exports an object with a certain identifier.
     * @param name The identifier for the exported object.
     * @param object The object to be exported.
     * @return Returns false when exporting failed, otherwise true.
     */
    bool exportObject(const QString &name, QObject *object);
    /**
     * @brief unexportObject removes an object from this QRPCObjectExporter.
     * @param name The identifier of the exported object that should be removed.
     * @return Returns true on success, else false.
     */
    bool unexportObject(const QString &name);

    /**
     * @brief Returns a identifier-object map of all exported objects.
     */
    ObjectMap exportedObjects() const;

signals:
    void dispatchSignal(QObject *sender, int id, void **o, QPrivateSignal);
    void dynamicPropertyChanged(QObject *object, const QByteArray &propertyName, const QVariant &value, QPrivateSignal);

    /**
     * @brief exportedObjectsChanged is emitted when an object was exported or unexported.
     * @param exportedObjects The updated map of exported objects.
     */
    void exportedObjectsChanged(const ObjectMap &exportedObjects);

protected:
    QRPCObjectExporter(QRPCObjectExporterPrivate &dd, QObject *parent = 0);
    bool eventFilter(QObject *watched, QEvent *event);

private:
    friend class QRPCSignalDispatcher;
    friend class QRPCServerPrivate;

    QRPCObjectExporterPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(QRPCObjectExporter)
};

#endif // QRPCOBJECTEXPORTER_H
