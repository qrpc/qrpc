/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCSERVER_H
#define QRPCSERVER_H

#include <QObject>
#include <QSet>
#include <QUuid>
#include <QVariant>

#include "qrpc_global.h"
#include "qrpctransport.h"

class QRPCServerPrivate;
class QRPCObjectExporter;

/**
 * @brief The QRPCServer class, serving exported objects over a transport to a client.
 *
 * Instantiate a QRPCServer with a QRPCObjectExporter and a QRPCTransport to handle all the work needed to transparently
 * proxy the QRPCObjectExporter's QObjects over the QRPCTransport to the client.
 */
class QRPC_EXPORT QRPCServer : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Constructs a QRPCServer.
     * @param exporter The exporter holding the exported objects.
     * @param transport The transport over which communication happens.
     * @param parent The parent object of this QRPCServer.
     */
    QRPCServer(QRPCObjectExporter *exporter, QRPCTransport *transport, QObject *parent = 0);
    ~QRPCServer();

    /// @brief Returns the QRPCObjectExporter instance associated with this QRPCServer.
    QRPCObjectExporter *exporter() const;

    /// @brief Returns the QRPCTransport instance associated with this QRPCServer.
    QRPCTransport *transport() const;

    /// @brief Returns the unique ID for this client-server connection.
    QUuid connectionId() const;

protected:
    QRPCServer(QRPCServerPrivate &dd, QObject *parent);

    QRPCServerPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(QRPCServer)
};

#endif // QRPCSERVER_H
