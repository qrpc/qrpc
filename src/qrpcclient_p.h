/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCCLIENT_P_H
#define QRPCCLIENT_P_H

#include <QByteArray>
#include <QMap>
#include <QString>
#include <QUuid>
#include <QVariant>

class QRPCClient;
class QRPCObject;
class QRPCTransport;

struct QRPCClientPrivate
{
    QRPCClientPrivate(QRPCClient *parent, QRPCTransport *transport);
    virtual ~QRPCClientPrivate();

    void handleMessage(const QString &objectId, QRPCTransport::MessageType type, const QVariant &payload);

    void updateConnectionId(const QVariant &payload);
    void initializeObject(const QString &objectId, const QVariant &payload);
    void dispatchSignal(const QString &objectId, const QVariant &payload);
    void updateDynamicProperty(const QString &objectId, const QVariant &payload);
    void writeDynamicProperty(QRPCObject *object, const QByteArray &propertyName, const QVariant &value);
    void invokeMetaMethod(QRPCObject *object, int id, void **o);
    void writeProperty(QRPCObject *object, int id, void **o);

    QRPCClient * const q_ptr;
    Q_DECLARE_PUBLIC(QRPCClient)

    QRPCTransport *transport;
    QMap<QString, QRPCObject*> remoteObjects;
    QMap<QByteArray, const QMetaObject*> metaObjects;
    QUuid connectionId;
};

#endif // QRPCCLIENT_P_H
