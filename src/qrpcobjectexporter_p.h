/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCOBJECTEXPORTER_P_H
#define QRPCOBJECTEXPORTER_P_H

#include "qrpcobjectexporter.h"

class QRPCSignalDispatcher : public QObject
{
public:
    QRPCSignalDispatcher(QObject *parent) : QObject(parent) {}

    int qt_metacall(QMetaObject::Call call, int id, void **o);
};

struct QRPCObjectExporterPrivate
{
    QRPCObjectExporterPrivate(QRPCObjectExporter *parent)
        : q_ptr(parent), signalDispatcher(new QRPCSignalDispatcher(parent))
    {}

    QRPCObjectExporter * const q_ptr;
    Q_DECLARE_PUBLIC(QRPCObjectExporter)

    QList<const QMetaObject*> metaObjectsForObject(const QString &name);

    QRPCObjectExporter::ObjectMap exportedObjects;
    QObject *signalDispatcher;
};

#endif // QRPCOBJECTEXPORTER_P_H
