/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCPASSTHROUGHTRANSPORT_H
#define QRPCPASSTHROUGHTRANSPORT_H

#include "qrpctransport.h"

/**
 * @brief The QRPCPassthroughTransport class simply relays all sent messsages to another QRPCTransport.
 *
 * Don't use a single QRPCPassthroughTransport when you want to use QRPC locally. Create a server->client and a
 * client->server transport and connect both with setTargetTransport.
 */
class QRPC_EXPORT QRPCPassthroughTransport : public QRPCTransport
{
public:
    QRPCPassthroughTransport(QObject *parent = 0);
    ~QRPCPassthroughTransport();

    /**
     * @brief Sets the target transport to which all messages are relayed.
     */
    void setTargetTransport(QRPCTransport *transport) { m_targetTransport = transport; }

    /**
     * @brief Implemented from QRPCTransport. Relays the message to the target transport directly.
     */
    void sendMessage(const QString &objectId, MessageType type, const QVariant &payload) override;

private:
    QRPCTransport *m_targetTransport;
};

#endif // QRPCPASSTHROUGHTRANSPORT_H
