QT -= gui

TARGET = qrpc
CONFIG   += c++11

win32: CONFIG += skip_target_version_ext

DEFINES += QRPC_LIBRARY

TEMPLATE = lib

VERSION = 2.0.0

SOURCES += \
    qmetaobjectbuilder/qmetaobjectbuilder.cpp \
    qrpctransport.cpp \
    qrpcserver.cpp \
    qrpcobjectexporter.cpp \
    qrpcclient.cpp \
    qrpcobject.cpp \
    qrpcpassthroughtransport.cpp \
    qrpciodevicetransport.cpp

PRIVATE_HEADERS += \
    qmetaobjectbuilder/qmetaobjectbuilder_p.h \
    qmetaobjectbuilder/qmetaobject_p.h \
    qrpcclient_p.h \
    qrpcobject_p.h \
    qrpcobjectexporter_p.h \
    qrpcserver_p.h \
    qrpciodevicetransport_p.h

PUBLIC_HEADERS +=  \
    qrpc_global.h \
    qrpctransport.h \
    qrpcserver.h \
    qrpcobjectexporter.h \
    qrpcclient.h \
    qrpcobject.h \
    qrpcpassthroughtransport.h \
    qrpciodevicetransport.h

HEADERS += \
    $$PRIVATE_HEADERS \
    $$PUBLIC_HEADERS \

isEmpty(INSTALL_PREFIX) {
    win32:INSTALL_PREFIX=C:/qrpc
    else:INSTALL_PREFIX=/usr
}

win32: target.path = $${INSTALL_PREFIX}/bin
else:  target.path = $${INSTALL_PREFIX}/lib
headers.files = $$PUBLIC_HEADERS
headers.path = $${INSTALL_PREFIX}/include/qrpc/
INSTALLS += target headers
