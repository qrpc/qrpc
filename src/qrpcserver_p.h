/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCSERVER_P_H
#define QRPCSERVER_P_H

#include <QString>
#include <QByteArray>
#include <QVariant>
#include <QSet>

#include "qrpctransport.h"

class QRPCObjectExporter;
class QRPCServer;

struct QRPCServerPrivate
{
    QRPCServerPrivate(QRPCServer *parent, QRPCObjectExporter *exporter, QRPCTransport *transport);
    ~QRPCServerPrivate();

    void handleMessage(const QString &objectId, QRPCTransport::MessageType type, const QVariant &payload);

    void dispatchSignal(QObject *sender, int id, void **o);
    void dispatchDynamicPropertyChange(QObject *object, const QByteArray &name, const QVariant &value);
    void sendConnectionUUID();
    void sendInitData(const QString &objectId);
    void invokeMethod(const QString &objectId, int methodId, QVariantList args);
    void writeProperty(const QString &objectId, int propertyId, const QVariant &value);
    void writeDynamicProperty(const QString &objectId, const QByteArray &propName, const QVariant &value);

    QRPCServer * const q_ptr;
    Q_DECLARE_PUBLIC(QRPCServer)

    QRPCObjectExporter *exporter;
    QRPCTransport *transport;
    QSet<const QMetaObject*> transmittedMetaObjects;
    QUuid connectionId;
    QList<QMetaObject::Connection> privConns;
};

#endif // QRPCSERVER_P_H
