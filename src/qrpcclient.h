/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCCLIENT_H
#define QRPCCLIENT_H

#include <QMap>
#include <QObject>
#include <QUuid>

#include "qrpc_global.h"
#include "qrpctransport.h"

class QRPCObject;

class QRPCClientPrivate;

/**
 * @brief The QRPCClient class requests remote objects and manages them locally.
 *
 * It communicates with the server part over a QRPCTransport and relays method invocations and property changes to the
 * server.
 */
class QRPC_EXPORT QRPCClient : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QUuid connectionId READ connectionId NOTIFY connectionIdChanged)
public:
    /**
     * @brief Constructs a new QRPCClient.
     * @param transport The transport over which communication happens with the server.
     * @param parent The parent object of this QRPCClient.
     */
    explicit QRPCClient(QRPCTransport *transport, QObject *parent = 0);
    ~QRPCClient();

    /// @brief Returns the QRPCTransport instance associated with this QRPCClient.
    QRPCTransport *transport() const;

    /**
     * @brief Requests a remote object from the server with the identifier 'name'.
     *
     * This method is non-blocking. When the object is ready, the signal remoteObjectReady() is emitted with the
     * remote object as its parameter.
     */
    void requestRemoteObject(const QString &name);

    /// @brief Returns the UUID for this client-server connection.
    QUuid connectionId() const;

    /// @brief Returns a map, keyed by the remote object ID, of all known remote objects
    QMap<QString, QRPCObject*> knownObjects() const;

signals:
    /**
     * @brief This signal is emitted when a new remote object is ready.
     */
    void remoteObjectReady(QRPCObject *remoteObject);
    void connectionIdChanged(const QUuid &connectionId);

protected:
    explicit QRPCClient(QRPCClientPrivate &dd, QObject *parent);

private:
    friend class QRPCObject;

    QRPCClientPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(QRPCClient)
};

#endif // QRPCCLIENT_H
