/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCOBJECT_P_H
#define QRPCOBJECT_P_H

#include <QMap>
#include <QString>
#include <QVariant>

class QMetaObject;

struct QRPCObjectPrivate
{
    QRPCObjectPrivate(const QString &objectId, const QMetaObject *fakeMetaObject,
                      const QMap<int, QVariant> &properties)
        : objectId(objectId), fakeMetaObject(fakeMetaObject), propertyValues(properties), initializing(true)
    {
    }

    QString objectId;
    const QMetaObject *fakeMetaObject;
    QMap<int, QVariant> propertyValues;
    bool initializing;
};

#endif // QRPCOBJECT_P_H
