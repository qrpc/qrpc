/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCIODEVICETRANSPORT_H
#define QRPCIODEVICETRANSPORT_H

#include "qrpc_global.h"
#include "qrpctransport.h"

class QIODevice;
class QRPCIODeviceTransportPrivate;

class QRPC_EXPORT QRPCIODeviceTransport : public QRPCTransport
{
public:
    explicit QRPCIODeviceTransport(QIODevice *device, QObject *parent = 0);
    ~QRPCIODeviceTransport();

    void sendMessage(const QString &objectId, MessageType type, const QVariant &payload) override;

protected:
    QRPCIODeviceTransport(QRPCIODeviceTransportPrivate &dd, QObject *parent);

    QRPCIODeviceTransportPrivate * const d_ptr;
    Q_DECLARE_PRIVATE(QRPCIODeviceTransport)
};

#endif // QRPCIODEVICETRANSPORT_H
