/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qrpcclient.h"
#include "qrpcclient_p.h"
#include "qrpcobject.h"
#include "qrpcobject_p.h"

#include <QEvent>
#include <QMetaProperty>

QRPCObject::QRPCObject(const QString &objectId, const QMetaObject *fakeMetaObject,
                       const QMap<int, QVariant> &properties, QRPCClient *parent)
    : QObject(parent), d_ptr(new QRPCObjectPrivate(objectId, fakeMetaObject, properties))
{
    for (int i = QObject::staticMetaObject.propertyCount(); i < fakeMetaObject->propertyCount(); ++i) {
        QMetaProperty prop = fakeMetaObject->property(i);

        if (!prop.hasNotifySignal() || prop.notifySignal().parameterCount() == 0) {
            continue;
        }

        QMetaObject::connect(this, prop.notifySignalIndex(),
                             this, fakeMetaObject->methodCount() + i - QObject::staticMetaObject.propertyCount());
    }
}

QRPCObject::~QRPCObject()
{
    delete d_ptr;
}

QRPCClient *QRPCObject::rpcClient()
{
    return static_cast<QRPCClient*>(parent());
}

const QMetaObject *QRPCObject::metaObject() const
{
    Q_D(const QRPCObject);

    return d->fakeMetaObject;
}

QString QRPCObject::remoteObjectId() const
{
    Q_D(const QRPCObject);
    return d->objectId;
}

bool QRPCObject::event(QEvent *event)
{
    Q_D(QRPCObject);

    if (event->type() == QEvent::DynamicPropertyChange && !d->initializing) {
        QDynamicPropertyChangeEvent *dpcEvent = static_cast<QDynamicPropertyChangeEvent*>(event);
        rpcClient()->d_ptr->writeDynamicProperty(this, dpcEvent->propertyName(), property(dpcEvent->propertyName()));
    }

    return QObject::event(event);
}

QRPCObject::QRPCObject(QRPCObjectPrivate &dd, QRPCClient *parent)
    : QObject(parent), d_ptr(&dd)
{
}

int QRPCObject::qt_metacall(QMetaObject::Call call, int id, void **o)
{
    Q_D(QRPCObject);

    // if the id falls outside our total method count, it is a property update signal
    if (call == QMetaObject::InvokeMetaMethod && id >= d->fakeMetaObject->methodCount()) {
        id -= d->fakeMetaObject->methodCount();
        QVariant &propVal = d->propertyValues[id];
        propVal = QVariant(propVal.userType(), o[1]);
        return -1;
    }

    id = QObject::qt_metacall(call, id, o);
    if (id < 0)
        return id;

    switch (call) {
    case QMetaObject::InvokeMetaMethod: {
        rpcClient()->d_ptr->invokeMetaMethod(this, id, o);
    } break;

    case QMetaObject::ReadProperty: {
        QVariant variant = d->propertyValues.value(id);
        QMetaType::destruct(variant.userType(), o[0]);
        QMetaType::construct(variant.userType(), o[0], variant.data());
    } break;

    case QMetaObject::WriteProperty: {
        rpcClient()->d_ptr->writeProperty(this, id, o);
    } break;

    default:
        break;
    }

    return -1;
}

