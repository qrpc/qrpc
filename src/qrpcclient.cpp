/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMetaMethod>
#include <QVariant>

#include <functional>

#include "qrpcclient.h"
#include "qrpcclient_p.h"
#include "qrpcobject.h"
#include "qrpcobject_p.h"

#include "qmetaobjectbuilder/qmetaobjectbuilder_p.h"

QRPCClient::QRPCClient(QRPCTransport *transport, QObject *parent)
    : QObject(parent), d_ptr(new QRPCClientPrivate(this, transport))
{
}

QRPCClient::~QRPCClient()
{
    delete d_ptr;
}

QRPCTransport *QRPCClient::transport() const
{
    Q_D(const QRPCClient);
    return d->transport;
}

void QRPCClient::requestRemoteObject(const QString &name)
{
    Q_D(QRPCClient);

    if (d->remoteObjects.contains(name)) {
        emit remoteObjectReady(d->remoteObjects.value(name));
    } else {
        d->transport->sendMessage(name, QRPCTransport::InitRequest, QVariant());
    }
}

QUuid QRPCClient::connectionId() const
{
    Q_D(const QRPCClient);

    return d->connectionId;
}

QMap<QString, QRPCObject *> QRPCClient::knownObjects() const
{
    Q_D(const QRPCClient);

    return d->remoteObjects;
}

QRPCClient::QRPCClient(QRPCClientPrivate &dd, QObject *parent)
    : QObject(parent), d_ptr(&dd)
{
}

void QRPCClientPrivate::handleMessage(const QString &objectId, QRPCTransport::MessageType type, const QVariant &payload)
{
    switch (type) {
    case QRPCTransport::SignalEmission: {
        dispatchSignal(objectId, payload);
    } break;

    case QRPCTransport::InitData: {
        initializeObject(objectId, payload);
    } break;

    case QRPCTransport::DynamicPropertyChange: {
        updateDynamicProperty(objectId, payload);
    } break;

    case QRPCTransport::ConncetionUUID: {
        updateConnectionId(payload);
    } break;

    case QRPCTransport::Error: {
        qWarning("QRPCClient: Received error from server: \"%s\"", qPrintable(payload.toString()));
    } break;

    default: {
        qWarning("QRPCClient: No handler for message type %d - this should not happen!", type);
    } break;
    }
}

void QRPCClientPrivate::updateConnectionId(const QVariant &payload)
{
    Q_Q(QRPCClient);
    connectionId = payload.toUuid();
    emit q->connectionIdChanged(connectionId);
}

QRPCClientPrivate::QRPCClientPrivate(QRPCClient *parent, QRPCTransport *transport)
    : q_ptr(parent), transport(transport)
{
    metaObjects.insert(QByteArrayLiteral("QObject"), &QObject::staticMetaObject);

    using namespace std::placeholders;
    QObject::connect(transport, &QRPCTransport::messageReady, parent,
                     std::bind(&QRPCClientPrivate::handleMessage, this, _1, _2, _3));
}

QRPCClientPrivate::~QRPCClientPrivate()
{
    for (const QMetaObject *mo : metaObjects.values()) {
        if (qstrcmp(mo->className(), "QObject") != 0) {
            free((void*)mo);
        }
    }
}

void QRPCClientPrivate::initializeObject(const QString &objectId, const QVariant &payload)
{
    Q_Q(QRPCClient);

    QVariantList variants = payload.toList();

    QByteArray metaObjectsData = variants[0].toByteArray();
    QByteArray metaObjectName = variants[1].toByteArray();

    QVariantList staticProperties = variants[2].toList();
    QVariantList dynamicProperties = variants[3].toList();

    QDataStream stream(&metaObjectsData, QIODevice::ReadOnly);
    stream.setVersion(QDataStream::Qt_5_0);

    while (!stream.atEnd()) {
        QRPCInternals::QMetaObjectBuilder builder;
        builder.deserialize(stream, metaObjects);

        if (stream.status() != QDataStream::Ok) {
            qWarning("QRPCClient::initializeObject: Failed to deserialize QMetaObject for class %s, aborting init.",
                     builder.className().constData());
            return;
        }

        QByteArray realClassName = builder.className();
        builder.setClassName(QByteArrayLiteral("Remote") + realClassName);

        metaObjects.insert(realClassName, builder.toMetaObject());
    }

    const QMetaObject *fakeMetaObject = metaObjects.value(metaObjectName);
    Q_ASSERT(fakeMetaObject);

    QMap<int, QVariant> properties;

    while (!staticProperties.isEmpty()) {
        int id = staticProperties.takeFirst().toInt();
        QVariant value = staticProperties.takeFirst();
        properties.insert(id, value);
    }

    QRPCObject *remoteObject = new QRPCObject(objectId, fakeMetaObject, std::move(properties), q);
    remoteObjects[objectId] = remoteObject;

    while (!dynamicProperties.isEmpty()) {
        QByteArray name = dynamicProperties.takeFirst().toByteArray();
        QVariant value = dynamicProperties.takeFirst();
        remoteObject->setProperty(name.constData(), value);
    }

    // initialization done, callbacks can now be enabled.
    remoteObject->d_ptr->initializing = false;

    emit q->remoteObjectReady(remoteObject);
}

void QRPCClientPrivate::dispatchSignal(const QString &objectId, const QVariant &payload)
{
    QObject *object = remoteObjects.value(objectId);
    if (!object) {
        return;
    }

    QVariantList variants = payload.toList();

    int id = variants.takeFirst().toInt() + QObject::staticMetaObject.methodCount();

    const QMetaObject *mo = object->metaObject();
    const QMetaMethod method = mo->method(id);

    Q_ASSERT(method.parameterCount() == variants.count());

    void **o = new void*[method.parameterCount() + 1];
    o[0] = method.returnType() == QMetaType::Void ? 0 : QMetaType::create(method.returnType());

    for (int i = 0; i < method.parameterCount(); ++i) {
        o[i + 1] = variants[i].data();
    }

    mo->activate(object, id, o);

    if (method.returnType() != QMetaType::Void) {
        QMetaType::destroy(method.returnType(), o[0]);
    }
}

void QRPCClientPrivate::updateDynamicProperty(const QString &objectId, const QVariant &payload)
{
    QObject *object = remoteObjects.value(objectId);
    if (!object) {
        return;
    }

    QVariantList list = payload.toList();

    object->setProperty(list[0].toByteArray(), list[1]);
}

void QRPCClientPrivate::writeDynamicProperty(QRPCObject *object, const QByteArray &propertyName, const QVariant &value)
{
    QVariantList payload;
    payload << propertyName << value;

    transport->sendMessage(object->remoteObjectId(), QRPCTransport::DynamicPropertyWrite, payload);
}

void QRPCClientPrivate::invokeMetaMethod(QRPCObject *object, int id, void **o)
{
    QVariantList payload;
    payload << static_cast<qint32>(id);

    const QMetaObject *mo = object->metaObject();
    const QMetaMethod method = mo->method(id + QObject::staticMetaObject.methodCount());

    for (int i = 0; i < method.parameterCount(); ++i) {
        payload << QVariant(method.parameterType(i), o[i + 1]);
    }

    transport->sendMessage(object->remoteObjectId(), QRPCTransport::MethodInvocation, payload);
}

void QRPCClientPrivate::writeProperty(QRPCObject *object, int id, void **o)
{
    QVariantList payload;
    payload << static_cast<qint32>(id);

    const QMetaObject *mo = object->metaObject();
    const QMetaProperty prop = mo->property(id + QObject::staticMetaObject.propertyCount());

    payload << QVariant(prop.type(), o[0]);

    transport->sendMessage(object->remoteObjectId(), QRPCTransport::PropertyWrite, payload);
}
