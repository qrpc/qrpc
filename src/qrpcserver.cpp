/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QMetaMethod>

#include <functional>

#include "qrpcobjectexporter.h"
#include "qrpcserver.h"
#include "qrpcserver_p.h"
#include "qrpctransport.h"
#include "qrpcobjectexporter_p.h"

#include "qmetaobjectbuilder/qmetaobjectbuilder_p.h"

QRPCServer::QRPCServer(QRPCObjectExporter *exporter, QRPCTransport *transport, QObject *parent)
    : QObject(parent), d_ptr(new QRPCServerPrivate(this, exporter, transport))
{
    d_ptr->sendConnectionUUID();
}

QRPCServer::~QRPCServer()
{
    delete d_ptr;
}

QRPCObjectExporter *QRPCServer::exporter() const
{
    Q_D(const QRPCServer);
    return d->exporter;
}

QRPCTransport *QRPCServer::transport() const
{
    Q_D(const QRPCServer);
    return d->transport;
}

QUuid QRPCServer::connectionId() const
{
    Q_D(const QRPCServer);
    return d->connectionId;
}

QRPCServer::QRPCServer(QRPCServerPrivate &dd, QObject *parent)
    : QObject(parent), d_ptr(&dd)
{

}

QRPCServerPrivate::QRPCServerPrivate(QRPCServer *parent, QRPCObjectExporter *exporter, QRPCTransport *transport)
    : q_ptr(parent), exporter(exporter), transport(transport)
{
    connectionId = QUuid::createUuid();

    using namespace std::placeholders;

    privConns << QObject::connect(exporter, &QRPCObjectExporter::dispatchSignal, parent,
                                  std::bind(&QRPCServerPrivate::dispatchSignal, this, _1, _2, _3));
    privConns << QObject::connect(exporter, &QRPCObjectExporter::dynamicPropertyChanged, parent,
                                  std::bind(&QRPCServerPrivate::dispatchDynamicPropertyChange, this, _1, _2, _3));
    privConns << QObject::connect(transport, &QRPCTransport::messageReady, parent,
                                  std::bind(&QRPCServerPrivate::handleMessage, this, _1, _2, _3));
}

QRPCServerPrivate::~QRPCServerPrivate()
{
    for (auto conn : privConns) {
        QObject::disconnect(conn);
    }
}

void QRPCServerPrivate::handleMessage(const QString &objectId, QRPCTransport::MessageType type, const QVariant &payload)
{
    switch (type) {
    case QRPCTransport::InitRequest: {
        sendInitData(objectId);
    } break;

    case QRPCTransport::MethodInvocation: {
        QVariantList args = payload.toList();
        int methodId = args.takeFirst().toInt();
        invokeMethod(objectId, methodId, std::move(args));
    } break;

    case QRPCTransport::PropertyWrite: {
        QVariantList args = payload.toList();
        int propertyId = args[0].toInt();
        writeProperty(objectId, propertyId, args[1]);
    } break;

    case QRPCTransport::DynamicPropertyWrite: {
        QVariantList args = payload.toList();
        writeDynamicProperty(objectId, args[0].toByteArray(), args[1]);
    } break;

    case QRPCTransport::Error: {
        qWarning("QRPCServer: Received error from client: \"%s\"", qPrintable(payload.toString()));
    } break;

    default: {
        qWarning("QRPCServer: No handler for message type %d - this should not happen!", type);
    } break;
    }
}

void QRPCServerPrivate::dispatchSignal(QObject *sender, int id, void **o)
{
    const QString objectName = exporter->exportedObjects().key(sender);
    Q_ASSERT(!objectName.isEmpty());

    QVariantList payload;
    payload << static_cast<qint32>(id - QObject::staticMetaObject.methodCount());

    const QMetaObject *mo = sender->metaObject();
    const QMetaMethod method = mo->method(id);

    for (int i = 0; i < method.parameterCount(); ++i) {
        payload << QVariant(method.parameterType(i), o[i + 1]);
    }

    transport->sendMessage(objectName, QRPCTransport::SignalEmission, payload);
}

void QRPCServerPrivate::dispatchDynamicPropertyChange(QObject *object, const QByteArray &name, const QVariant &value)
{
    const QString objectName = exporter->exportedObjects().key(object);
    Q_ASSERT(!objectName.isEmpty());

    QVariantList payload;
    payload << name << value;

    transport->sendMessage(objectName, QRPCTransport::DynamicPropertyChange, payload);
}

void QRPCServerPrivate::sendConnectionUUID()
{
    transport->sendMessage(QString(), QRPCTransport::ConncetionUUID, connectionId);
}

void QRPCServerPrivate::sendInitData(const QString &objectId)
{
    if (!exporter->exportedObjects().contains(objectId)) {
        transport->sendMessage(QString(), QRPCTransport::Error,
                                 QString("Unknown object \"%1\" requested").arg(objectId));
        return;
    }

    // get the complete meta object list
    QList<const QMetaObject*> neededMetaObjects = exporter->d_ptr->metaObjectsForObject(objectId);

    // filter out the ones that we've already transmitted
    foreach (const QMetaObject *mo, neededMetaObjects) {
        if (transmittedMetaObjects.contains(mo)) {
            neededMetaObjects.removeOne(mo);
        }
    }

    QByteArray metaObjectsData;
    QDataStream stream(&metaObjectsData, QIODevice::WriteOnly);
    stream.setVersion(QDataStream::Qt_5_0);

    QRPCInternals::QMetaObjectBuilder::AddMembers everythingButStaticMetaCall
        = QRPCInternals::QMetaObjectBuilder::AllMembers;
    everythingButStaticMetaCall &= ~QRPCInternals::QMetaObjectBuilder::StaticMetacall;

    // serialize everything into the byte array
    for (const QMetaObject *mo: neededMetaObjects) {
        QRPCInternals::QMetaObjectBuilder builder;
        builder.addMetaObject(mo, everythingButStaticMetaCall);
        builder.setClassName(builder.className());
        builder.setFlags(QRPCInternals::QMetaObjectBuilder::DynamicMetaObject);
        builder.serialize(stream);
        transmittedMetaObjects.insert(mo);
    }

    QObject *object = exporter->exportedObjects().value(objectId);
    const QMetaObject *mo = object->metaObject();

    QVariantList payload;
    payload << metaObjectsData
            << QByteArray(mo->className());

    QVariantList staticPropertyValues;
    for (int i = QObject::staticMetaObject.propertyCount(); i < mo->propertyCount(); ++i) {
        QMetaProperty prop = mo->property(i);
        staticPropertyValues << static_cast<qint32>(prop.propertyIndex() - QObject::staticMetaObject.propertyCount())
                             << prop.read(object);
    }

    payload << QVariant(staticPropertyValues);

    QVariantList dynamicPropertyValues;
    for (const QByteArray &name : object->dynamicPropertyNames()) {
        dynamicPropertyValues << name << object->property(name.constData());
    }

    payload << QVariant(dynamicPropertyValues);

    transport->sendMessage(objectId, QRPCTransport::InitData, payload);
}

void QRPCServerPrivate::invokeMethod(const QString &objectId, int methodId, QVariantList args)
{
    if (!exporter->exportedObjects().contains(objectId)) {
        transport->sendMessage(QString(), QRPCTransport::Error,
                                 QString("Tried to invoke method with id %1 on unknown object \"%2\"")
                                        .arg(objectId));
        return;
    }

    QObject *object = exporter->exportedObjects().value(objectId);

    const QMetaObject *mo = object->metaObject();
    const QMetaMethod method = mo->method(methodId + QObject::staticMetaObject.methodCount());

    void **o = new void*[method.parameterCount() + 1];

    o[0] = method.returnType() == QMetaType::Void ? 0 : QMetaType::create(method.returnType());

    for (int i = 0; i < method.parameterCount(); ++i) {
        o[i + 1] = args[i].data();
    }

    int ret = object->qt_metacall(QMetaObject::InvokeMetaMethod, method.methodIndex(), o);
    Q_ASSERT(ret < 0);
    Q_UNUSED(ret);

    QMetaType::destroy(method.returnType(), o[0]);

    delete[] o;
}

void QRPCServerPrivate::writeProperty(const QString &objectId, int propertyId, const QVariant &value)
{
    if (!exporter->exportedObjects().contains(objectId)) {
        transport->sendMessage(QString(), QRPCTransport::Error,
                                 QString("Tried to set property with id %1 on unknown object \"%2\"")
                                        .arg(propertyId).arg(objectId));
        return;
    }

    QObject *object = exporter->exportedObjects().value(objectId);

    const QMetaObject *mo = object->metaObject();
    const QMetaProperty prop = mo->property(propertyId + QObject::staticMetaObject.propertyCount());

    prop.write(object, value);
}

void QRPCServerPrivate::writeDynamicProperty(const QString &objectId, const QByteArray &propName, const QVariant &value)
{
    if (!exporter->exportedObjects().contains(objectId)) {
        transport->sendMessage(QString(), QRPCTransport::Error,
                                 QString("Tried to set dynamic property with name %1 on unknown object \"%2\"")
                                        .arg(QString::fromUtf8(propName)).arg(objectId));
        return;
    }

    QObject *object = exporter->exportedObjects().value(objectId);
    object->setProperty(propName.constData(), value);
}
