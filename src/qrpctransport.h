/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef QRPCTRANSPORT_H
#define QRPCTRANSPORT_H

#include <QObject>

#include "qrpc_global.h"

class QRPC_EXPORT QRPCTransport : public QObject
{
    Q_OBJECT
public:
    enum MessageType : qint8 {
        Error = 0,
        InitRequest,
        InitData,
        SignalEmission,
        MethodInvocation,
        PropertyWrite,
        DynamicPropertyWrite,
        DynamicPropertyChange,
        ConncetionUUID,
    };
    Q_ENUM(MessageType)

    explicit QRPCTransport(QObject *parent = 0);
    ~QRPCTransport();

signals:
    void messageReady(const QString &objectId, MessageType type, const QVariant &payload);

public slots:
    virtual void sendMessage(const QString &objectId, MessageType type, const QVariant &payload) = 0;
};

#endif // QRPCTRANSPORT_H
