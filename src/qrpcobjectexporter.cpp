/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 * Copyright (C) 2016 Menlo Systems GmbH
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#include <QEvent>
#include <QMetaMethod>

#include "qrpcobjectexporter.h"
#include "qrpcobjectexporter_p.h"

#include "qmetaobjectbuilder/qmetaobjectbuilder_p.h"

QRPCObjectExporter::QRPCObjectExporter(QObject *parent)
    : QObject(parent), d_ptr(new QRPCObjectExporterPrivate(this))
{
}

QRPCObjectExporter::QRPCObjectExporter(QRPCObjectExporterPrivate &dd, QObject *parent)
    : QObject(parent), d_ptr(&dd)
{
}

QRPCObjectExporter::~QRPCObjectExporter()
{
    delete d_ptr;
}

bool QRPCObjectExporter::exportObject(const QString &name, QObject *object)
{
    Q_D(QRPCObjectExporter);

    if (name.isEmpty()) {
        qWarning("QRPCObjectExporter::exportObject: name must not be empty");
        return false;
    }

    if (d->exportedObjects.contains(name)) {
        qWarning("QRPCObjectExporter::exportObject: Object with identifier %s already exported.", qPrintable(name));
        return false;
    }

    d->exportedObjects.insert(name, object);

    const QMetaObject *mo = object->metaObject();

    for (int i = QObject::staticMetaObject.methodCount(); i < mo->methodCount(); ++i) {
            QMetaMethod meth = mo->method(i);
            if (meth.methodType() != QMetaMethod::Signal) {
                continue;
            }

            QMetaObject::connect(object, i, d->signalDispatcher, QObject::staticMetaObject.methodCount() + i);
    }

    object->installEventFilter(this);

    emit exportedObjectsChanged(d->exportedObjects);

    return true;
}

bool QRPCObjectExporter::unexportObject(const QString &name)
{
    Q_D(QRPCObjectExporter);

    if (!d->exportedObjects.contains(name)) {
        qWarning("QRPCObjectExporter::unexportObject: No object with identifier %s exported.", qPrintable(name));
        return false;
    }

    QObject *object = d->exportedObjects.value(name);
    const QMetaObject *mo = object->metaObject();

    for (int i = QObject::staticMetaObject.methodCount(); i < mo->methodCount(); ++i) {
            QMetaMethod meth = mo->method(i);
            if (meth.methodType() != QMetaMethod::Signal) {
                continue;
            }

            QMetaObject::disconnect(object, i, d->signalDispatcher, QObject::staticMetaObject.methodCount() + i);
    }

    object->removeEventFilter(this);

    d->exportedObjects.remove(name);
    emit exportedObjectsChanged(d->exportedObjects);
    return true;
}

QRPCObjectExporter::ObjectMap QRPCObjectExporter::exportedObjects() const
{
    Q_D(const QRPCObjectExporter);

    return d->exportedObjects;
}

static void accumulateMetaObjects(const QMetaObject *mo, QList<const QMetaObject*>& list)
{
    if (mo == &QObject::staticMetaObject || list.contains(mo))
        return;

    accumulateMetaObjects(mo->superClass(), list);
    const QMetaObject * const * rmo = mo->d.relatedMetaObjects;
    if (rmo) while (*rmo) {
        accumulateMetaObjects(*rmo, list);
        ++rmo;
    }
    list.append(mo);
}

bool QRPCObjectExporter::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::DynamicPropertyChange) {
        QDynamicPropertyChangeEvent *dpcEvent = static_cast<QDynamicPropertyChangeEvent*>(event);
        emit dynamicPropertyChanged(watched, dpcEvent->propertyName(), watched->property(dpcEvent->propertyName()),
                                    QPrivateSignal());
    }

    return QObject::eventFilter(watched, event);
}

QList<const QMetaObject*> QRPCObjectExporterPrivate::metaObjectsForObject(const QString &name)
{
    if (!exportedObjects.contains(name)) {
        qWarning("QRPCObjectExporterPrivate::metaObjectsForObject: No object with identifier %s exported.",
                 qPrintable(name));
        return QList<const QMetaObject*>();
    }

    QList<const QMetaObject*> neededMetaObjects;
    accumulateMetaObjects(exportedObjects.value(name)->metaObject(), neededMetaObjects);

    return neededMetaObjects;
}

int QRPCSignalDispatcher::qt_metacall(QMetaObject::Call call, int id, void **o) {
    id = QObject::qt_metacall(call, id, o);
    if (id < 0) return id;

    QRPCObjectExporter *exporter = static_cast<QRPCObjectExporter*>(parent());
    exporter->dispatchSignal(QObject::sender(), id, o, QRPCObjectExporter::QPrivateSignal());

    return -1;
}
