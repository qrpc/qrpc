/*
 * QRPC - Remote Procedure Call library using the Qt metaobject system.
 * Copyright (C) 2015-2016 Arno Rehn <arno@arnorehn.de>
 *
 * QRPC is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * QRPC is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with QRPC. If not, see <http://www.gnu.org/licenses/>.
 */

#include "qrpcpassthroughtransport.h"

#include <QtDebug>

QRPCPassthroughTransport::QRPCPassthroughTransport(QObject *parent)
    : QRPCTransport(parent), m_targetTransport(0)
{

}

QRPCPassthroughTransport::~QRPCPassthroughTransport()
{

}

void QRPCPassthroughTransport::sendMessage(const QString &objectId, QRPCTransport::MessageType type,
                                           const QVariant &payload)
{
    emit m_targetTransport->messageReady(objectId, type, payload);
}
