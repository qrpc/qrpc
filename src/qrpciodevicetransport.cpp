#include "qrpciodevicetransport.h"
#include "qrpciodevicetransport_p.h"

#include <QBuffer>
#include <QDataStream>
#include <QIODevice>
#include <QVariant>
#include <QVector>

#include <functional>

struct Message
{
    QString objectId;
    QRPCTransport::MessageType type;
    QVariant payload;
};

QDataStream &operator<<(QDataStream &stream, const Message &message)
{
    stream << message.objectId << qint8(message.type) << message.payload;
    return stream;
}

QDataStream &operator>>(QDataStream &stream, Message &message)
{
    qint8 type;
    stream >> message.objectId >> type >> message.payload;
    message.type = QRPCIODeviceTransport::MessageType(type);
    return stream;
}


QRPCIODeviceTransport::QRPCIODeviceTransport(QIODevice *device, QObject *parent)
    : QRPCTransport(parent), d_ptr(new QRPCIODeviceTransportPrivate(this, device))
{

}

QRPCIODeviceTransport::~QRPCIODeviceTransport()
{

}

void QRPCIODeviceTransport::sendMessage(const QString &objectId, QRPCTransport::MessageType type,
                                        const QVariant &payload)
{
    Q_D(QRPCIODeviceTransport);

    Message msg = { objectId, type, payload };

    QBuffer buf;
    buf.open(QBuffer::ReadWrite);

    QDataStream stream(&buf);
    stream.setVersion(QDataStream::Qt_5_0);
    stream << msg;

    stream.setDevice(d->device);
    stream.writeBytes(buf.data(), buf.data().size());
}

QRPCIODeviceTransport::QRPCIODeviceTransport(QRPCIODeviceTransportPrivate &dd, QObject *parent)
    : QRPCTransport(parent), d_ptr(&dd)
{

}

QRPCIODeviceTransportPrivate::QRPCIODeviceTransportPrivate(QRPCIODeviceTransport *parent, QIODevice *device)
    : q_ptr(parent), device(device), nextMessageSize(0)
{
    QObject::connect(device, &QIODevice::readyRead, parent, std::bind(&QRPCIODeviceTransportPrivate::readData, this),
                     Qt::QueuedConnection);
}

QRPCIODeviceTransportPrivate::~QRPCIODeviceTransportPrivate()
{
}

void QRPCIODeviceTransportPrivate::readData()
{
    QVector<Message> messages;
    Message message;

    while (readMessage(message)) {
        messages.append(message);
    }

    Q_Q(QRPCIODeviceTransport);
    for (const Message &msg : messages) {
        emit q->messageReady(msg.objectId, msg.type, msg.payload);
    }
}

bool QRPCIODeviceTransportPrivate::readMessage(Message &msg)
{
    QDataStream stream(device);
    stream.setVersion(QDataStream::Qt_5_0);

    // Try to read the size of the next message if we don't have it already.
    if (!nextMessageSize) {
        // if we don't even have enough data available to read the size, return.
        if (device->bytesAvailable() < qint64(sizeof(quint32))) {
            return false;
        }

        stream >> nextMessageSize;
    }

    // we got the messages size, but maybe we now don't have enough data for the rest of the message.
    if (device->bytesAvailable() < nextMessageSize) {
        return false;
    }

    // else, just read the message and be done.
    stream >> msg;

    nextMessageSize = 0;

    return true;
}
