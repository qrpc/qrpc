# QRPC: A Qt remoting library

## Overview

QRPC enables remoting via Qt's meta-object system.

QMetaObjects of "exported" QObjects are serialised and sent from server to
client, where a specialised QObject is created with the transmitted QMetaObject
as its dynamic meta-object. From the point of view of the meta-object system,
the client's object is a perfect replica. Method invocations, signal emissions
and property changes are serialised and transmitted from one side to the other.

Communication between client and server happens over user definable transports.
A passthrough transport and a QIODevice based one (thus covering Qt's network
sockets) are already included in QRPC.

## License

QRPC is free software and released under the terms of the GNU LGPLv3.

## Architectural remarks

### Serialisation of values

Serialisation happens via QVariant and QDataStream. Hence, custom types need
to be registered with qRegisterMetaType() and
qRegisterMetaTypeStreamOperators().
Serialisation of "QObject*" pointers is not supported even though the object
may be exported. However, we can't define custom stream operators for that type.
Serialisation could be done by manually inspecting the transmitted parameters,
but that can negatively impact performance.
Workarounds are easy, however.

### Implementation of properties

The system supports static QMetaProperties as well QObject's dynamic properties.
Only property *changes* are transmitted. As such, only QMetaProperties with
a notify signal are properly supported.

### Method return values

Return values of signals or slots are intentionally not supported because it
would require more complex tracking of state and involve blocking method calls.
